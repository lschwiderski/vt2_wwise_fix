use anyhow::Context;
use anyhow::Result;

use std::env;
use std::ffi::{OsStr, OsString};
use std::fs;
use std::fs::File;
use std::io::{Seek, SeekFrom, Write};
use std::path::Path;

// Offset within the bank file to the version number within the BKHD section.
// TODO: Check if this may change.
const OFFSET_VERSION_NUMBER: u64 = 52;
// Retrieved by philipdestroyer by inspecting the game's sound banks in memory at runtime.
const BANK_VERSION: [u8; 4] = [0x28, 0xbc, 0x11, 0x92];

fn fix_file<P>(path: P) -> Result<()>
where
    P: AsRef<Path>,
{
    let mut f = File::options()
        .write(true)
        .read(true)
        .open(&path)
        .with_context(|| "failed to open sound bank file")?;

    f.seek(SeekFrom::Start(OFFSET_VERSION_NUMBER))
        .with_context(|| "failed to seek to version number offset")?;
    f.write(&BANK_VERSION)
        .with_context(|| "failed to write version number")?;

    Ok(())
}

fn main() -> Result<()> {
    let dir = env::args()
        .nth(1)
        .expect("Please specify a directory containing .wwise_bank files");

    let mut results = Vec::new();

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();

        if path.is_file() && path.extension().and_then(OsStr::to_str) == Some("wwise_bank") {
            let res = fix_file(&path);
            results.push((path, res));
        }
    }

    for (path, res) in results.iter() {
        let name = path
            .file_name()
            .map(OsStr::to_os_string)
            .unwrap_or_else(|| OsString::from(".."));

        match res {
            Ok(_) => println!("Fixed: {}", name.to_string_lossy()),
            Err(err) => eprintln!("Failed: {}. Error: {:#}", name.to_string_lossy(), err),
        }
    }

    Ok(())
}
